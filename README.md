What is j3?
===========

j3 is a JavaScript DOM toolkit deriving ideas from jQuery, d3, knockout
and others.
A j3.group is equivalent to a jQuery instance, while a j3.selection provides
similar capabilities as d3.
In contrast to these other toolkits, j3 places an emphasis on recursive DOM
generation in order to improve performance.

Example
-------

Here's some example code to create a table of contents for a HTML document:

    var headings = j3.range(6).each(function (i) { return "h" + (1 + i); }).join(",");

    function tocLevel(toc) {
        j3(toc).selectAll(":scope > ol").
            data(function (d) { return [ d ]; }).
            call(function () {
                this.enter().append("ol");
                this.exit().remove();
            }).
            selectAll(":scope > li").
            data(function (d) {
                return j3(d || "body").selectAll(":scope > section").nodes();
            }).
            call(function () {
                this.enter().append("li").append("a");
                this.exit().remove();
                this.select("a").
                    attr("href", function (d, i) {
                        var id = d.getAttribute("id");
                        if (null == id || "" === id) {
                            id = (d.parentNode.getAttribute("id") || "toc") + "." + i;
                            j3(d).attr("id", id);
                        }
                        return "#" + id;
                    }).
                    text(function (d) {
                        return j3(d).select(headings).text();
                    });
            }).
            each(function (d) {
                tocLevel(this);
            });
    }
    tocLevel("#toc");
