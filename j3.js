var j3 = (function () {
    "use strict";

    var DATA = "__j3__";

    function isArrayLike(a) {
	return "string" !== typeof a && null != a && "number" === typeof a.length;
    }

    function processNode(node, i) {
	return null !== node ? this.call(node, node[DATA], i) : null;
    }

    function invokeOr(val, func) {
	return  "function" === typeof val ? val : func;
    }

    function valueGetter(value) {
	return function () {
	    return value;
	};
    }

    var alwaysTrue = valueGetter(true);

    var invocable = function (value) {
	return invokeOr(value, valueGetter(value));
    };

    /**
     * I mix in the ability to hold a collection of nodes. The nodes aren't
     * required to be actual DOM nodes.
     *
     * I provide 'each', 'some' and 'every' for iteration.
     *
     * The argument isn't copied defensively and may be modified afterwards.
     * This happens to the update selection when the corresponding enter
     * selection is modified.
     */
    var nodeList = function (m_nodes, self) {
	self = self || {};

	self.size = function () { return m_nodes.length; };
	self.nodes = function () { return m_nodes.slice(); };

	self.each = function (func) {
	    return m_nodes.map(processNode.bind(func));
	};

	self.some = function (func) {
	    var r = false;
	    m_nodes.some(function () {
		return r = processNode.apply(func, arguments);
	    });
	    return r;
	};

	self.every = function (func) {
	    var r = true;
	    m_nodes.every(function () {
		return r = processNode.apply(func, arguments);
	    });
	    return r;
	};

	return self;
    };

    /**
     * I mix in actions that are useful for collections of both pseudo nodes
     * and DOM nodes.
     *
     * The base class should implement 'each' and 'some' for iteration and
     * 'select' which applies a function to each node.
     */
    var nodeActions = function (self) {
	self = self || {};

	self.empty = function () {
	    return !self.some(alwaysTrue);
	};

	var pick = self.pick = function (func) {
	    var r = null;
	    self.some(function () {
		r = func.apply(this, arguments);
		return true;
	    });
	    return r;
	};

	self.append = function (elem) {
	    var makeElem = invokeOr(
		elem,
		function () { return this.ownerDocument.createElement(elem); }
	    );

	    return self.select(function () {
		return this.appendChild(makeElem.apply(this, arguments));
	    });
	};

	self.datum = function (d) {
	    if (!arguments.length) {
		return pick(identity);
	    }

	    d = invocable(d);
	    self.each(function () {
		this[DATA] = d.apply(this, arguments);
	    });
	    return self;
	};

	return self;
    };

    /**
     * A collection of pseudo-nodes for representing the enter selection.
     *
     * Added nodes are inserted into the update selection's node array.
     */
    var enterGroup = function (m_parent, m_nodes, m_update) {
	var self = nodeActions(nodeList(m_nodes));
	var each = self.each;

	self.parent = valueGetter(m_parent);
	self.select = function (selector) {
	    return group(
		m_parent,
		each(function (d, i) {
		    var node = selector.apply(m_parent, arguments) || null;
		    if (null !== node) {
			m_update[i] = node;
			node[DATA] = d;
		    }
		    return node;
		})
	    );
	};

	return self;
    };

    /**
     * I mix in actions for DOM nodes.
     *
     * The base class should provide 'filter', 'each' and 'some' for iteration.
     */
    var domActions = function (self) {
	self = nodeActions(self || {});
	var pick = self.pick;

	// Prevent memory leaks
	var cleanNode = function (node) {
	    if (Node.ELEMENT_NODE === node.nodeType || Node.DOCUMENT_NODE === node.nodeType) {
		node[DATA] = undefined;
	    }
	    return cleanChildren(node);
	};

	var cleanChildren = function (node) {
	    return Array.prototype.forEach.call(node.querySelectorAll("*"), cleanNode);
	};

	self.has = function (node) {
	    return self.filter(
		node instanceof Node
	      ? function () {
		    var current = node;
		    while (current instanceof Node) {
			if (this.isSameNode ? this.isSameNode(current) : this === current) {
			    return true;
			}
			current = current.parentNode;
		    }
		    return false;
		}
	      : function () { return null !== this.querySelector(node); }
	    );
	};

	self.remove = function () {
	    self.each(function () {
		cleanNode(this);
		if (null !== this.parentNode) {
		    this.parentNode.removeChild(this);
		}
	    });
	    return self;
	};

	self.attr = function (attr, value) {
	    if (arguments.length < 2) {
		return pick(function () { return this.getAttribute(attr); });
	    }

	    value = invocable(value);
	    self.each(function () {
		this.setAttribute(attr, value.apply(this, arguments));
	    });
	    return self;
	};

	self.text = function (text) {
	    if (!arguments.length) {
		return pick(function () { return this.textContent; });
	    }

	    text = invocable(text);
	    self.each(function () {
		cleanChildren(this);
		this.textContent = text.apply(this, arguments);
	    });
	    return self;
	};

	self.classed = function (name, value) {
	    if (arguments.length < 2) {
		return pick(function () { return this.classList.contains(name); });
	    }

	    name = name.split(' ');
	    value = invocable(value);
	    self.each(function () {
	        this.classList[value.apply(this, arguments) ? 'add' : 'remove'].
		    apply(this.classList, name);
	    });
	    return self;
	};

	return self;
    };

    /**
     * A collection of DOM nodes, grouped under a common parent node.
     */
    function group(m_parent, m_nodes) {
	var self = domActions(nodeList(m_nodes));
	var nodes = self.nodes;
	var each = self.each;

	self.parent = valueGetter(m_parent);
	self.filter = function (selector) {
	    var sel = invokeOr(
		selector,
		function () { return this.matches(selector); }
	    );

	    return group(
		m_parent,
		m_nodes.filter(processNode.bind(sel))
	    );
	};

	self.select = function (selector) {
	    var sel = invokeOr(
		selector,
		function () { return this.querySelector(selector); }
	    );

	    return group(
		m_parent,
		each(function (d) {
		    var node = sel.apply(this, arguments) || null;
		    if (null !== node) {
			node[DATA] = d;
		    }
		    return node;
		})
	    );
	};

	self.selectAll = function (selector) {
	    var sel = invokeOr(
		selector,
		function () { return this.querySelectorAll(selector); }
	    );

	    return selection(each(function () {
		return group(
		    this,
		    Array.prototype.slice.call(sel.apply(this, arguments))
		);
	    }));
	};

	self.data = function (values, key, j) {
	    var enter, update, exit, nodesByKey;

	    if (!arguments.length) {
		return m_nodes.map(function (node) {
		    return node ? node[DATA] : void 0;
		});
	    }

	    if ("function" === typeof key) {
		nodesByKey = (function () {
		    var haveKeys = {};
		    each(function (d, i) {
			haveKeys[key.apply(this, arguments)] = i;
		    });
		    return function (k) {
			return haveKeys.hasOwnProperty(k) ? haveKeys[k] : void 0;
		    };
		}());
	    } else {
		key = function (d, i) { return i; };
		nodesByKey = function (k) {
		    return 0 <= k && k < m_nodes.length ? k : void 0;
		};
	    }

	    enter = [];
	    update = [];
	    exit = nodes();
	    values = invocable(values);
	    Array.prototype.forEach.call(
		values.call(m_parent, m_parent[DATA], j),
		function (d, i, val) {
		    var node, i = nodesByKey(key.call(val, d, i));

		    if (void 0 === i || null === exit[i]) {
			node = {};
			enter.push(node);
			update.push(null);
		    } else {
			node = exit[i];
			delete exit[i];
			enter.push(null);
			update.push(node);
		    }
		    node[DATA] = d;
		}
	    );

	    enter = enterGroup(m_parent, enter, update);
	    update = group(m_parent, update);
	    exit = group(m_parent, exit);

	    update.enter = valueGetter(enter);
	    update.exit = valueGetter(exit);

	    return update;
	};

	return self;
    }

    function selection(m_groups) {
	var self = domActions();

	var eachGroup = function (func) {
	    return m_groups.map(function (grp, j) {
		return func.call(grp, grp.parent(), j);
	    });
	};

	var transform = function (func) {
	    return selection(eachGroup(func));
	};

	var someGroup = function (func) {
	    var r = false;
	    m_groups.some(function (grp, j) {
		return r = func.call(grp, grp.parent(), j);
	    });
	    return r;
	};

	var pickGroup = function (func) {
	    var r = null;
	    someGroup(function () {
		r = func.apply(this, arguments);
		return true;
	    });
	    return r;
	};

	var everyGroup = function (func) {
	    var r = true;
	    m_groups.every(function (grp, j) {
		return r = func.call(grp, grp.parent(), j);
	    });
	    return r;
	};

	var each = self.each = function (func) {
	    return eachGroup(function () { return this.each(func); });
	};

	self.some = function (func) {
	    return someGroup(function () { return this.some(func); });
	};

	self.every = function (func) {
	    return everyGroup(function () { return this.every(func); });
	};

	self.nodes = function () {
	    return merge(each(function () { return this; }));
	};

	self.call = function (arg0) {
	    var func = arg0;
	    arg0 = self;
	    func.apply(self, arguments);
	    return self;
	};

	self.filter = function (selector) {
	    return transform(function () { return this.filter(selector); });
	};

	self.select = function (selector) {
	    return transform(function () { return this.select(selector); });
	};

	self.selectAll = function (selector) {
	    var sel = invokeOr(
		selector,
		function () { return this.querySelectorAll(selector); }
	    );

	    return selection(merge(each(function () {
		return group(
		    this,
		    Array.prototype.slice.call(sel.apply(this, arguments))
		);
	    })));
	};

	self.data = function (values, key) {
	    var groups, sel;

	    if (!arguments.length) {
		return pickGroup(function () { return this.data(); });
	    }

	    groups = eachGroup(function (parent, j) {
		return this.data(values, key, j);
	    });

	    sel = selection(groups);
	    sel.enter = valueGetter(
		selection(groups.map(function (grp) { return grp.enter(); })));
	    sel.exit = valueGetter(
		selection(groups.map(function (grp) { return grp.exit(); })));

	    return sel;
	};

	return self;
    }

    var identity = self.identity = function (a) {
	return a;
    };

    var merge = self.merge = function (arrays) {
	return Array.prototype.concat.apply([], arrays);
    };

    self.range = function (start, stop, step) {
	var self = {};

	if (arguments.length < 2) {
	    stop = start;
	    start = 0;
	}
	if (arguments.length < 3) {
	    step = 1;
	}
	if (0 == step) {
	    throw new RuntimeError("0 == step");
	}

	self.each = 0 < step
		  ? function (func) {
			var res = [];
			for (; start < stop; start += step) {
			    res.push(func(start));
			}
			return res;
		    }
		  : function (func) {
			var res = [];
			for (; stop < start; start += step) {
			    res.push(func(start));
			}
			return res;
		    };

	return self;
    };

    self.select = function (node) {
	return group(document.documentElement, [
	    node instanceof Node ? node
				 : document.querySelector(node)
	]);
    };

    function self(nodes) {
	return group(
	    document.documentElement,
	      void 0 === nodes      ? [ document.documentElement ]
	    : nodes instanceof Node ? [ nodes ]
	    // Copy mutable argument defensively, and convert NodeList to Array.
	    : Array.prototype.slice.call(
		isArrayLike(nodes)  ? nodes
				    : document.querySelectorAll(nodes)
	      )
	);
    };

    return self.selectAll = self;
}());

